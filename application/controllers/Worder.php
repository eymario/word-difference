<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worder extends CI_Controller {
	public function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->model('WorderModel');
    }

	public function index() {
		if($this->input->method() == 'post') {
			if(!empty($_FILES['document']['name'])) {
				$filename = $_FILES['document']['name'];
				$document = '';
				$type = '';

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				if($ext == 'doc') {
					$document = $this->read_doc($filename);
					$type = 'document';
				} else if($ext == 'docx') {
					$document = $this->read_docx($filename);
					$type = 'document';
				} else if($ext == 'pptx') {
					$document = $this->read_pptx($filename);
					$type = 'presentation';
				}

				$name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
				$slug = url_title($name);

				$this->WorderModel->insert_document($name, $document, $type, $slug);

				$this->load->view('index');
			}

			if(!empty($_FILES['difference']['name'])) {
				$filename = $_FILES['difference']['name'];
				$document = '';
				$difference = '';

				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				if($ext == 'doc') {
					$document = $this->read_doc($filename);
				} else if($ext == 'docx') {
					$document = $this->read_docx($filename);
				}

				$documents = $this->WorderModel->get_documents();

				$smallest_diff_count = 0;
				$i = 0;

				foreach($documents AS $variation) {
					$current_diff_count = $this->diff_count($variation['content'], $document);

					if($i == 0) {
						$smallest_diff_count = $current_diff_count;
						$difference = $this->html_diff($variation['content'], $document);
					} else {
						if($current_diff_count < $smallest_diff_count) {
							$smallest_diff_count = $current_diff_count;
							$difference = $this->html_diff($variation['content'], $document);
						}
					}

					$i++;
				}

				$data['document'] = $document;
				$data['difference'] = $difference;

				$this->load->view('difference', $data);
			}

			if(!empty($_POST['search'])) {
				$documents = $this->WorderModel->search_documents($_POST['string']);

				$data['documents'] = $documents;
				$data['string'] = $_POST['string'];

				$this->load->view('search', $data);
			}
		} else {
			$this->load->view('index');
		}
	}

	public function single_document($document_slug) {
		$data['document'] = $this->WorderModel->get_document($document_slug);
		$data['string'] = $_GET['string'];

		$this->load->view('single', $data);
	}

	private function diff($old, $new){
		$matrix = array();
		$maxlen = 0;
		foreach($old as $oindex => $ovalue){
			$nkeys = array_keys($new, $ovalue);
			foreach($nkeys as $nindex){
				$matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
				$matrix[$oindex - 1][$nindex - 1] + 1 : 1;
				if($matrix[$oindex][$nindex] > $maxlen){
					$maxlen = $matrix[$oindex][$nindex];
					$omax = $oindex + 1 - $maxlen;
					$nmax = $nindex + 1 - $maxlen;
				}
			}   
		}
		if($maxlen == 0) return array(array('d' => $old, 'i' => $new));
		return array_merge(
			$this->diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
			array_slice($new, $nmax, $maxlen),
			$this->diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
	}

	private function diff_count($old, $new) {
		$ret = '';
		$diff = $this->diff(preg_split("/[\s]+/", $old), preg_split("/[\s]+/", $new));
		$counter = 0;

		foreach($diff as $k){
			if(is_array($k)) {
				$counter++;
			}
		}
		
		return $counter;
	}

	private function html_diff($old, $new) {
		$ret = '';
		$diff = $this->diff(preg_split("/[ ]+|\n/", $old), preg_split("/[ ]+|\n/", $new));

		foreach($diff as $k){
			if(is_array($k))
				$ret .= (!empty($k['d'])?"<del>".implode(' ',$k['d'])."</del> ":'').
			(!empty($k['i'])?"<ins>".implode(' ',$k['i'])."</ins> ":'');
			else $ret .= $k . ' ';
		}
		
		return $ret;
	}

	private function read_docx($filename) {
		$striped_content = '';
		$content = '';

		$zip = zip_open($filename);

		if (!$zip || is_numeric($zip)) return false;

		while ($zip_entry = zip_read($zip)) {

			if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

			if (zip_entry_name($zip_entry) != "word/document.xml") continue;

			$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

			zip_entry_close($zip_entry);
		}

		zip_close($zip);

		$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
		$content = str_replace('</w:r></w:p>', "\r\n", $content);
		$content = str_replace("</w:p>", "\r\n", $content);

		$content = str_replace("Ä‘", "đ", $content);

		$striped_content = strip_tags($content);

		return $striped_content;
	}

	private function read_doc($filename) {
		if(file_exists($filename)) {
			if(($fh = fopen($filename, 'r')) !== false ) {
				$headers = fread($fh, 0xA00);

				// 1 = (ord(n)*1) ; Document has from 0 to 255 characters
				$n1 = ( ord($headers[0x21C]) - 1 );

				// 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743 characters
				$n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );

				// 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423 characters
				$n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );

				// 1 = (((ord(n)*256)*256)*256) ; Document has from 16775424 to 4294965504 characters
				$n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );

				// Total length of text in the document
				$textLength = ($n1 + $n2 + $n3 + $n4);

				$extracted_plaintext = fread($fh, $textLength);
				$extracted_plaintext = mb_convert_encoding($extracted_plaintext,'UTF-32');

				//$extracted_plaintext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/", "", $extracted_plaintext);

				return $extracted_plaintext;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// private function read_pptx($filename) {
 //    // This approach uses detection of the string "chr(0f).Hex_value.chr(0x00).chr(0x00).chr(0x00)" to find text strings, which are then terminated by another NUL chr(0x00). [1] Get text between delimiters [2] 
	// 	$striped_content = '';
	// 	$content = '';

	// 	$zip = zip_open($filename);

	// 	while ($zip_entry = zip_read($zip)) {

	// 		if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

	// 		//if (zip_entry_name($zip_entry) != "word/document.xml") continue;

	// 		$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

	// 		zip_entry_close($zip_entry);
	// 	}

	// 	zip_close($zip);

	// 	$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
	// 	$content = str_replace('</w:r></w:p>', "\r\n", $content);
	// 	$content = str_replace("</w:p>", "\r\n", $content);

	// 	// remove pptx sample data
	// 	$content = str_replace("Tekst naslova", "", $content);
	// 	$content = str_replace("Tijelo razine jedan", "", $content);
	// 	$content = str_replace("Tijelo razine dva", "", $content);
	// 	$content = str_replace("Tijelo razine tri", "", $content);
	// 	$content = str_replace("Tijelo razine četiri", "", $content);
	// 	$content = str_replace("Tijelo razine pet", "", $content);
	// 	$content = str_replace("“Citat upišite ovdje.”", "", $content);
	// 	$content = str_replace("–Ivan Horvat", "", $content);

	// 	$content = str_replace("Ä‘", "đ", $content);

	// 	$striped_content = strip_tags($content);

	// 	return $striped_content;
	// }

	private function read_pptx($filename) {
		$zip_handle = new ZipArchive;
		$output_text = "";

		if(true === $zip_handle->open($filename)){
			$slide_number = 1;

			while(($xml_index = $zip_handle->locateName("ppt/slides/slide".$slide_number.".xml")) !== false){
				$xml_datas = $zip_handle->getFromIndex($xml_index);
				$xml_handle = new DOMDocument();
				$xml_handle->loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);

				$output_text .= '<b>Slide '.$slide_number.'</b>';
				$output_text .= strip_tags($xml_handle->saveXML());
				
				$slide_number++;
			}
			$zip_handle->close();
		} else {
			$output_text .="";
		}

		return $output_text;
	}
}
