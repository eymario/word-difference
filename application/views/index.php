<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<title>Worder</title>

		<style>
			body {
				background-color: #efefef;
			}

			h1 {
				text-align: center;
			}

			form {
				margin-bottom: 20px;
			}

			ins {
				font-weight: bold;
				color: green;
			}
			del {
				text-decoration: line-through;
				color: red;
			}

			.Difference {
				display: flex;
				width: 90%;
				max-width: 1200px;
				margin: 0 auto;
			}

			.Difference-document {
				width: calc(50% - 40px);
				height: 80vh;
				overflow: scroll;
				background: white;
				margin: 0 20px;
				padding: 15px;
			}

			.highlight {
				color: red;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<h1>Word to text</h1>

		<form name="upload" action="" method="POST" enctype="multipart/form-data">
			<label for="document">Dokument za upload u bazu</label>
			<br/>
			<input type="file" name="document" id="document">
			<input type="hidden" name="upload" value="upload">
			<button type="submit">Spremi</button>
		</form>

		<form name="difference" action="" method="POST" enctype="multipart/form-data">
			<label for="difference">Dokument za usporedbu</label>
			<br/>
			<input type="file" name="difference" id="difference">
			<input type="hidden" name="upload" value="upload">
			<button type="submit">Provjeri</button>
		</form>

		<form name="search" action="" method="POST">
			<label for="string">Pretraga dokumenata</label>
			<br/>
			<textarea name="string" id="string"></textarea>
			<input type="hidden" name="search" value="search">
			<button type="submit">Provjeri</button>
		</form>
	</body>
</html>