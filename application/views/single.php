<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<title>Worder</title>

		<style>
			body {
				background-color: #efefef;
			}

			h1 {
				text-align: center;
			}

			form {
				margin-bottom: 20px;
			}

			ins {
				font-weight: bold;
				color: green;
			}
			del {
				text-decoration: line-through;
				color: red;
			}

			.Difference {
				display: flex;
				width: 90%;
				max-width: 1200px;
				margin: 0 auto;
			}

			.Difference-document {
				width: calc(50% - 40px);
				height: 80vh;
				overflow: scroll;
				background: white;
				margin: 0 20px;
				padding: 15px;
			}

			.highlight {
				color: red;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<h1>Word to text</h1>
		
		<div class="Difference">
		<?php

			$final_document = preg_replace("/\w*?$string\w*/i", '<span class="highlight">$0</span>', $document[0]['content']);
		?>
		<div class="Difference-document">
			<?= nl2br($final_document) ?>
		</div>
	</body>
</html>