<?php
class WorderModel extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function insert_document($name, $content, $type, $slug) {
		$data = array(
			'name' => $name,
			'content' => $content,
			'type' => $type,
			'slug' => $slug
		);

		$this->db->insert('documents', $data);
	}

	public function get_documents() {
		$this->db->select('content');
		$this->db->from('documents');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function search_documents($string) {
		$this->db->select('name, slug');
		$this->db->from('documents');
		$this->db->like('content', $string);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_document($document_slug) {
		$this->db->select('name, content');
		$this->db->from('documents');
		$this->db->where('slug', $document_slug);
		$query = $this->db->get();

		return $query->result_array();
	}
}
