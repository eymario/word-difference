<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<title>Worder</title>

		<style>
			body {
				background-color: #efefef;
			}

			h1 {
				text-align: center;
			}

			ins {
				font-weight: bold;
				color: green;
			}
			del {
				text-decoration: line-through;
				color: red;
			}

			.Difference {
				display: flex;
				width: 90%;
				max-width: 1200px;
				margin: 0 auto;
			}

			.Difference-document {
				width: calc(50% - 40px);
				height: 80vh;
				overflow: scroll;
				background: white;
				margin: 0 20px;
				padding: 15px;
			}

			.highlight {
				color: red;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<h1>Word to text</h1>

		<div class="Difference">
			<div class="Difference-document">
				<?= nl2br($document) ?>
			</div>
			<div class="Difference-document">
				<?= nl2br($difference) ?>
			</div>
		</div>
	</body>
</html>