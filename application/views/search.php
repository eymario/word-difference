<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<title>Worder</title>

		<style>
			body {
				background-color: #efefef;
			}

			h1 {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<h1>Word to text</h1>
		
		<?php
			if(!empty($documents)) {
		?>
			<ul class="Documents">
		<?php
				foreach($documents AS $document) {
		?>
				<li><a href="/documents/<?= $document['slug'] ?>?string=<?= $string ?>"><?= $document['name'] ?></a></li>
		<?php
				}
			} else {
		?>
				<p>Nema rezultata</p>
		<?php
			}
		?>
	</body>
</html>