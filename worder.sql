-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 26, 2020 at 12:11 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `worder`
--

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `content`, `type`, `slug`, `date_added`) VALUES
(1, 'dokument2', '\nErste&amp;Steiermärkische Bank d.d., Rijeka, Jadranski trg 3a (dalje u tekstu: Banka), temeljem članka 1039 Zakona o obveznim odnosima (Narodne novine 35/05, 41/08, 125/11, 78/15, 29/2018) po nalogu i za račun društva Jagodica Bobica d.o.o., Velika ulica 12, 10000 Zagreb, matični broj 1234567, OIB 13131313131, račun broj HR122402060061100110011 zastupanog po direktoru Jagoda Jagodić (dalje u tekstu: Nalogodavac), u korist Mali dućan d.o.o., Mala ulica 1, 10000 Zagreb, matični broj 7654321, OIB 34343434343 (dalje u tekstu: Korisnik garancije) izdaje bankarsko jamstvo sljedećeg sadržaja:: \r\n\r\n\r\n\r\nG A R A N C I J A  br. 5402020304\r\n(dalje u tekstu: Garancija)\r\n\r\n\r\nUtvrđuje se da su Nalogodavac i Korisnik garancije sklopili Ugovor o prodaji stolica br. 287 od 31.08.2019. godine (dalje u tekstu: Ugovor).\r\n\r\nU skladu s odredbama Ugovora, čl. 12. st. 7. Nalogodavac je obvezan Korisniku garancije dostaviti Garanciju za izvršenje obveze plaćanja na iznos od 125.000,00 HRK i rok 01.09.2020. godine. \r\n\r\nOvom garancijom Banka se obvezuje da će, ukoliko Nalogodavac ne izvrši obvezu plaćanja iz Ugovora prema Korisniku garancije, na pisani, dokumentirani i opravdani poziv Korisnika garancije, najkasnije u roku 7 dana od dana zaprimanja istog, platiti svaki iznos ili iznose koji u ukupnoj vrijednosti mogu iznositi najviše\r\n\r\n= 125.000,00 HRK \r\n(slovima: stodvadesetpettisuća HRK)\r\n\r\nPoziv na plaćanje po ovoj Garanciji mora sadržavati pisanu izjavu Korisnika garancije da Nalogodavac nije ispunio obvezu prema Korisniku garancije koja proizlazi iz Ugovora te u kojoj Korisnik garancije mora navesti i koje obveze Nalogodavac nije ispunio sukladno Ugovoru te ih mora dokumentirati slijedećim prilozima: kopije računa za koje se traži plaćanje na kojima je vidljivo da je obveza Nalogodavca dospjela i da su fakture proizašle iz Ugovora te kopija opomene za plaćanje upućene Nalogodavcu od strane Korisnika garancije kojom se traži ispunjenje obveze uz rok od najmanje 8 dana za ispunjenje iste, te dokaz da je Nalogodavac primio opomenu, a što se dokazuje originalom povratnice ovjerene od Nalogodavca. Poziv na plaćanje mora biti u pisanoj formi na poslovnom pismu Korisnika garancije i potpisan od strane osoba ovlaštenih za zastupanje Korisnika garancije. Poziv se dostavlja preporučenom pošiljkom ili direktno na urudžbeni zapisnik Banke na adresu Erste &amp; Steiermärkische bank d.d., Direkcija poravnanja kreditnih poslova, Haulikova 19 a, 43000 Bjelovar.\r\n\r\nOva garancija stupa na snagu od datuma izdavanja.\r\n\r\nOva garancija važi najkasnije do 01.09.2020. godine i svaki zahtjev za plaćanje po Garanciji mora biti dostavljen Banci unutar tog roka. Smatrat će se da je poziv za plaćanje po Garanciji dostavljen u roku ako je zaprimljen u Banci, na gore naznačenoj adresi, najkasnije dana 01.09.2020. godine. Nakon isteka roka valjanosti Garancije prestaju sve obveze Banke prema Korisniku garancije bez obzira da li je originalni primjerak garancije vraćen ili je ostao u posjedu Korisnika garancije.\r\n\r\nOva Garancija prenosiva je samo uz prethodnu pisanu suglasnost Banke.\r\n\r\n\r\n\r\nU Zagrebu, 04.09.2019.\r\nErste&amp;Steiermärkische Bank d.d.\r\n\n', 'document', 'dokument2', '2020-03-16 14:07:27'),
(2, 'dokument2 kopija', '\nErste&amp;Steiermärkische Bank d.d., Rijeka, Jadranski trg 3a (dalje u tekstu: Banka), temeljem članka 1039 Zakona o obveznim odnosima (Narodne novine 35/05, 41/08, 125/11, 78/15, 29/2018) po nalogu i za račun društva Jagodica Bobica d.o.o., Velika ulica 12, 10000 Zagreb, matični broj 1234567, OIB 13131313131, račun broj HR122402060061100110011 zastupanog po direktoru Jagoda Jagodić (dalje u tekstu: Nalogodavac), u korist Mali dućan d.o.o., Mala ulica 1, 10000 Zagreb, matični broj 7654321, OIB 34343434343 (dalje u tekstu: Korisnik garancije) izdaje bankarsko jamstvo sljedećeg sadržaja:: \r\n\r\n\r\n\r\nG A R A N C I J A  br. 352934524\r\n(dalje u tekstu: Garancija)\r\n\r\n\r\nUtvrđuje se da su Nalogodavac i Korisnik garancije sklopili Ugovor o prodaji stolica br. 287 od 31.08.2019. godine (dalje u tekstu: Ugovor).\r\n\r\nU skladu s odredbama Ugovora, čl. 12. st. 7. Nalogodavac je obvezan Korisniku garancije dostaviti Garanciju za izvršenje obveze plaćanja na iznos od 125.000,00 HRK i rok 02.10.2020. godine. \r\n\r\nOvom garancijom Banka se obvezuje da će, ukoliko Nalogodavac ne izvrši obvezu plaćanja iz Ugovora prema Korisniku garancije, na pisani, dokumentirani i opravdani poziv Korisnika garancije, najkasnije u roku 7 dana od dana zaprimanja istog, platiti svaki iznos ili iznose koji u ukupnoj vrijednosti mogu iznositi najviše\r\n\r\n= 131.000,00 HRK \r\n(slovima: stotridesetjednatisuća HRK)\r\n\r\nPoziv na plaćanje po ovoj Garanciji mora sadržavati pisanu izjavu Korisnika garancije da Nalogodavac nije ispunio obvezu prema Korisniku garancije koja proizlazi iz Ugovora te u kojoj Korisnik garancije mora navesti i koje obveze Nalogodavac nije ispunio sukladno Ugovoru te ih mora dokumentirati slijedećim prilozima: kopije računa za koje se traži plaćanje na kojima je vidljivo da je obveza Nalogodavca dospjela i da su fakture proizašle iz Ugovora te kopija opomene za plaćanje upućene Nalogodavcu od strane Korisnika garancije kojom se traži ispunjenje obveze uz rok od najmanje 8 dana za ispunjenje iste, te dokaz da je Nalogodavac primio opomenu, a što se dokazuje originalom povratnice ovjerene od Nalogodavca. Poziv na plaćanje mora biti u pisanoj formi na poslovnom pismu Korisnika garancije i potpisan od strane osoba ovlaštenih za zastupanje Korisnika garancije. Poziv se dostavlja preporučenom pošiljkom ili direktno na urudžbeni zapisnik Banke na adresu Erste &amp; Steiermärkische bank d.d., Direkcija poravnanja kreditnih poslova, Haulikova 19 a, 43000 Bjelovar.\r\n\r\nOva garancija stupa na snagu od datuma izdavanja.\r\n\r\nOva garancija važi najkasnije do 01.09.2020. godine i svaki zahtjev za plaćanje po Garanciji mora biti dostavljen Banci unutar tog roka. Smatrat će se da je poziv za plaćanje po Garanciji dostavljen u roku ako je zaprimljen u Banci, na gore naznačenoj adresi, najkasnije dana 01.09.2020. godine. Nakon isteka roka valjanosti Garancije prestaju sve obveze Banke prema Korisniku garancije bez obzira da li je originalni primjerak garancije vraćen ili je ostao u posjedu Korisnika garancije.\r\n\r\nOva Garancija prenosiva je samo uz prethodnu pisanu suglasnost Banke.\r\n\r\n\r\n\r\nU Zagrebu, 04.09.2019.\r\nErste&amp;Steiermärkische Bank d.d.\r\n\n', 'document', 'dokument2-kopija', '2020-03-16 14:07:54'),
(3, 'dokument2 kopija2', '\nErste&amp;Steiermärkische Bank d.d., Rijeka, Jadranski trg 3a (dalje u tekstu: Banka), temeljem članka 1039 Zakona o obveznim odnosima (Narodne novine 35/05, 41/08, 125/11, 78/15, 29/2018) po nalogu i za račun društva Jagodica Bobica d.o.o., Velika ulica 12, 10000 Zagreb, matični broj 1234567, OIB 13131313131, račun broj HR122402060061100110011 zastupanog po direktoru Jagoda Jagodić (dalje u tekstu: Nalogodavac), u korist Mali dućan d.o.o., Mala ulica 1, 10000 Zagreb, matični broj 7654321, OIB 34343434343 (dalje u tekstu: Korisnik garancije) izdaje bankarsko jamstvo sljedećeg sadržaja:: \r\n\r\n\r\n\r\nG A R A N C I J A  br. 352934524\r\n(dalje u tekstu: Garancija)\r\n\r\n\r\nUtvrđuje se da su Nalogodavac i Korisnik garancije sklopili Ugovor o prodaji stolica br. 287 od 31.08.2019. godine (dalje u tekstu: Ugovor).\r\n\r\nU skladu s odredbama Ugovora, čl. 12. st. 7. Nalogodavac je obvezan Korisniku garancije dostaviti Garanciju za izvršenje obveze plaćanja na iznos od 125.000,00 HRK i rok 02.10.2020. godine. \r\n\r\nOvom garancijom Banka se obvezuje da će, ukoliko Nalogodavac ne izvrši obvezu plaćanja iz Ugovora prema Korisniku garancije, na pisani, dokumentirani i opravdani poziv Korisnika garancije, najkasnije u roku 7 dana od dana zaprimanja istog, platiti svaki iznos ili iznose koji u ukupnoj vrijednosti mogu iznositi najviše\r\n\r\n= 132.000,00 HRK \r\n(slovima: stotridesetjednatisuća HRK)\r\n\r\nPoziv na plaćanje po ovoj Garanciji mora sadržavati pisanu izjavu Korisnika garancije da Nalogodavac nije ispunio obvezu prema Korisniku garancije koja proizlazi iz Ugovora te u kojoj Korisnik garancije mora navesti i koje obveze Nalogodavac nije ispunio sukladno Ugovoru te ih mora dokumentirati slijedećim prilozima: kopije računa za koje se traži plaćanje na kojima je vidljivo da je obveza Nalogodavca dospjela i da su fakture proizašle iz Ugovora te kopija opomene za plaćanje upućene Nalogodavcu od strane Korisnika garancije kojom se traži ispunjenje obveze uz rok od najmanje 8 dana za ispunjenje iste, te dokaz da je Nalogodavac primio opomenu, a što se dokazuje originalom povratnice ovjerene od Nalogodavca. Poziv na plaćanje mora biti u pisanoj formi na poslovnom pismu Korisnika garancije i potpisan od strane osoba ovlaštenih za zastupanje Korisnika garancije. Poziv se dostavlja preporučenom pošiljkom ili direktno na urudžbeni zapisnik Banke na adresu Erste &amp; Steiermärkische bank d.d., Direkcija poravnanja kreditnih poslova, Haulikova 19 a, 43000 Bjelovar.\r\n\r\nOva garancija stupa na snagu od datuma izdavanja.\r\n\r\nOva garancija važi najkasnije do 01.09.2020. godine i svaki zahtjev za plaćanje po Garanciji mora biti dostavljen Banci unutar tog roka. Smatrat će se da je poziv za plaćanje po Garanciji dostavljen u roku ako je zaprimljen u Banci, na gore naznačenoj adresi, najkasnije dana 01.09.2020. godine. Nakon isteka roka valjanosti Garancije prestaju sve obveze Banke prema Korisniku garancije bez obzira da li je originalni primjerak garancije vraćen ili je ostao u posjedu Korisnika garancije.\r\n\r\nOva Garancija prenosiva je samo uz prethodnu pisanu suglasnost Banke.\r\n\r\n\r\n\r\nU Zagrebu, 04.09.2019.\r\nErste&amp;Steiermärkische Bank d.d.\r\n\n', 'document', 'dokument2-kopija2', '2020-03-16 14:08:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
